<?php
/**
 *
 * @description CobroDigital Payment Ticket Checkout Success Block
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Block\Checkout\Onepage\Success;

use Cmdepi\CobroDigital\Model\Sales\Order;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order\Config;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\App\Http\Context              as HttpContext;
use Cmdepi\CobroDigital\Model\Cd\Ticket             as TicketPaymentModel;
use Cmdepi\CobroDigital\Helper\PaymentTicket        as PaymentTicketHelper;

class Ticket extends \Magento\Checkout\Block\Onepage\Success
{
    /**
     *
     * @var Order
     *
     */
    protected $_order = null;

    /**
     *
     * @var PaymentTicketHelper
     *
     */
    protected $_paymentTicketHelper;

    /**
     *
     * Constructor
     *
     * @param PaymentTicketHelper $paymentTicketHelper
     * @param TemplateContext $context
     * @param Session $checkoutSession
     * @param Config $orderConfig
     * @param HttpContext $httpContext
     * @param array $data
     *
     */
    public function __construct(
        PaymentTicketHelper $paymentTicketHelper,
        TemplateContext $context,
        Session $checkoutSession,
        Config $orderConfig,
        HttpContext $httpContext,
        array $data = []
    ) {
        $this->_paymentTicketHelper = $paymentTicketHelper;

        parent::__construct(
            $context,
            $checkoutSession,
            $orderConfig,
            $httpContext,
            $data
        );
    }

    /**
     *
     * Validate if it is a 'payment ticket' order
     *
     * @return bool
     *
     */
    public function isPaymentTicketOrder()
    {
        $payment = $this->_getOrder()->getPayment();

        if ($payment->getMethod() == TicketPaymentModel::CODE) {
            return true;
        }

        return false;
    }

    /**
     *
     * Get download ticket URL
     *
     * @return string
     *
     */
    public function getPaymentTicketPrintUrl()
    {
        $paymentTicket   = $this->_getOrder()->getPaymentTicket();
        $paymentTicketId = $paymentTicket->getId();
        $urlPath         = sprintf('cmdepicd/sales/printticket/ticket/%s', $paymentTicketId);

        return $this->getUrl($urlPath);
    }

    /**
     *
     * Get ifram HTML content
     *
     * @return string
     *
     */
    public function getTicketHtml()
    {
        $paymentTicketHtml = $this->_getPaymentTicketHtml();
        return $this->_paymentTicketHelper->getBodyContentFromTicketHtml($paymentTicketHtml);
    }

    /**
     *
     * Get payment ticket from order
     *
     * @return string
     *
     */
    protected function _getPaymentTicketHtml()
    {
        return $this->_getOrder()->getPaymentTicket()->getHtml();
    }

    /**
     *
     * Get order
     *
     * @return Order
     *
     */
    protected function _getOrder()
    {
        if (is_null($this->_order)) {
            $this->_order = $this->_checkoutSession->getLastRealOrder();
        }

        return $this->_order;
    }
}
