<?php
/**
 *
 * @description Resource model to work with 'cd_payment_ticket' table
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Cmdepi\CobroDigital\Model\CdFactory;
use Cmdepi\CobroDigital\Model\Cd;
use Cmdepi\WorkingDays\Model\Date           as WorkingDate;
use Cmdepi\WorkingDays\Model\DateFactory    as WorkingDateFactory;
use Cmdepi\CobroDigital\Model\PaymentTicket as PaymentTicketModel;

class PaymentTicket extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * Default Payment Ticket Concept
     *
     * @const DEFAULT_CONCEPT
     *
     */
    const DEFAULT_CONCEPT = 'Magento Ticket';

    /**
     *
     * CobroDigital Model
     *
     * @var CdFactory
     *
     */
    protected $_cdFactory;

    /**
     *
     * Working Date Factory Model
     *
     * @var WorkingDateFactory
     *
     */
    protected $_workingDateFactory;

    /**
     *
     * @var DateTime
     *
     */
    protected $_date;

    /**
     *
     * CobroDigital Model
     *
     * @var Cd
     *
     */
    protected $_cd = null;

    /**
     *
     * Working Date Model
     *
     * @var WorkingDate
     *
     */
    protected $_workingDate = null;

    /**
     *
     * Constructor
     *
     * @param CdFactory $cdFactory
     * @param WorkingDateFactory $workingDateFactory
     * @param DateTime $date
     * @param Context $context
     * @param string $connectionName
     *
     */
    public function __construct(
        CdFactory $cdFactory,
        WorkingDateFactory $workingDateFactory,
        DateTime $date,
        Context $context,
        $connectionName = null
    )
    {
        /**
         *
         * Set CobroDigital factory model to interact with CobroDigital API
         *
         */
        $this->_cdFactory = $cdFactory;

        /**
         *
         * Set Working Date factory model
         *
         */
        $this->_workingDateFactory = $workingDateFactory;

        /**
         *
         * Get date
         *
         */
        $this->_date = $date;

        /**
         *
         * Call parent constructor
         *
         */
        parent::__construct($context, $connectionName);
    }

    /**
     *
     * Initialize resource model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init('cd_payment_ticket', 'entity_id');
    }

    /**
     *
     * Perform actions before object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        /**
         *
         * @note If payment ticket exists validate that its data is not saved because the payment ticket information has to remain unchanged
         *
         */
        if (!$object->isObjectNew()) {
            $object->unsetData(PaymentTicketModel::NRO_BOLETA);
            $object->unsetData(PaymentTicketModel::CREATED_AT);
            $object->unsetData(PaymentTicketModel::DUE_DATE);
            $object->unsetData(PaymentTicketModel::AMOUNT);
            $object->unsetData(PaymentTicketModel::PAYER_ID);
        }
        else {
            /** @var \Cmdepi\CobroDigital\Model\Payer $payer */
            $payer = $object->getPayer();

            /**
             *
             * @note Create CobroDigital payment ticket
             *
             */
            $dueDate   = $this->_calculateDueDate();
            $nroBoleta = $this->_getCd()->createPaymentTicket($payer->getIdentifierAttribute(), $payer->getIdentifier(), self::DEFAULT_CONCEPT, array($dueDate), array($object->getAmount()));

            /**
             *
             * @note Set CobroDigital payment ticket created date & due date
             *
             */
            $object->setData(PaymentTicketModel::DUE_DATE, $dueDate);
            $object->setData(PaymentTicketModel::CREATED_AT, $this->_date->date('Ymd'));

            /**
             *
             * @note Set CobroDigital payment ticket ID
             *
             */
            $object->setNroBoleta($nroBoleta);
        }

        return parent::_beforeSave($object);
    }

    /**
     *
     * Calculate due date
     *
     * @return string
     *
     */
    protected function _calculateDueDate()
    {
        $dueDate = $this->_getWorkingDate()->getNextWorkingDate($this->_getExpirationDays());
        return $dueDate->format('Ymd');
    }

    /**
     *
     * Get expiration days to calculate due date
     *
     * @return int
     *
     */
    protected function _getExpirationDays()
    {
        return $this->_getCd()->getConfigData(PaymentTicketModel::PAYMENT_TICKET_CONFIG_GROUP_ID, 'days_to_expiration');
    }

    /**
     *
     * Get WorkingDate Model
     *
     * @return WorkingDate
     *
     */
    protected function _getWorkingDate()
    {
        if (is_null($this->_workingDate)) {
            $this->_workingDate = $this->_workingDateFactory->create();
        }

        return $this->_workingDate;
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return Cd
     *
     */
    protected function _getCd()
    {
        if (is_null($this->_cd)) {
            $this->_cd = $this->_cdFactory->create();
        }

        return $this->_cd;
    }
}