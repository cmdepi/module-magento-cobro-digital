<?php
/**
 *
 * @description Module Logger
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Logger;

class Logger extends \Monolog\Logger
{}