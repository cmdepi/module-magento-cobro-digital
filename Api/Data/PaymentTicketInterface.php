<?php
/**
 *
 * @description Payment Ticket Model Interface
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Api\Data;

interface PaymentTicketInterface
{
    /**
     *
     * Payment Ticket ID
     *
     * @const ID
     *
     */
    const ID = 'entity_id';

    /**
     *
     * CobroDigital Payment Ticket Identifier
     *
     * @const NRO_BOLETA
     *
     */
    const NRO_BOLETA = 'nro_boleta';

    /**
     *
     * Payment Ticket Created Date
     *
     * @const CREATED_AT
     *
     */
    const CREATED_AT = 'created_at';

    /**
     *
     * Payment Ticket Due Date
     *
     * @const DUE_DATE
     *
     */
    const DUE_DATE = 'due_date';

    /**
     *
     * Payment Ticket Amount
     *
     * @const AMOUNT
     *
     */
    const AMOUNT = 'amount';

    /**
     *
     * Payment Ticket related Payer ID
     *
     * @const PAYER_ID
     *
     */
    const PAYER_ID = 'payer_id';

    /**
     *
     * Get payment ticket ID
     *
     * @return int|null
     *
     */
    public function getId();

    /**
     *
     * Get payment ticket ID
     *
     * @param int $id
     *
     * @return $this
     *
     */
    public function setId($id);

    /**
     *
     * Get CobroDigital Payment Ticket Identifier
     *
     * @return int|string
     *
     */
    public function getNroBoleta();

    /**
     *
     * Set CobroDigital Payment Ticket Identifier
     *
     * @param int|string $nroBoleta
     *
     * @return $this
     *
     */
    public function setNroBoleta($nroBoleta);

    /**
     *
     * Get Payment Ticket Created At
     *
     * @return string
     *
     * @note It does not have a set method because it is calculated before save action
     *
     */
    public function getCreatedAt();

    /**
     *
     * Get Payment Ticket Due Date
     *
     * @return string
     *
     * @note It does not have a set method because it is calculated before save action
     * @note It has the 'Ymd' format
     *
     */
    public function getDueDate();

    /**
     *
     * Get Payment Ticket Amount
     *
     * @return int
     *
     */
    public function getAmount();

    /**
     *
     * Set Payment Ticket Amount
     *
     * @param int $amount
     *
     * @return $this
     *
     */
    public function setAmount($amount);

    /**
     *
     * Get Payment Ticket Payer ID
     *
     * @return int|string
     *
     */
    public function getPayerId();

    /**
     *
     * Set Payment Ticket Payer Id
     *
     * @param int|string $payerId
     *
     * @return $this
     *
     */
    public function setPayerId($payerId);
}
