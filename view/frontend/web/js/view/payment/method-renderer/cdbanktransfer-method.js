/**
 *
 * @description CobroDigital Bank Transfer Payment Method KnockoutJS Logic
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
define(
    [
        'ko',
        'Magento_Checkout/js/view/payment/default',
        'jquery'
    ],
    function (ko, Component, $) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cmdepi_CobroDigital/payment/cdbanktransfer'
            },

            getPaymentInstallments: function () {
                return window.checkoutConfig.payment.cdbanktransfer.paymentinstallments;
            },

            /**
             *
             * Data to return when place order happened
             *
             * @returns {{method: (*|String), additional_data: {payment_method_nonce: (paymentMethodNonce|String)}}}
             *
             * @note Send data to
             *
             */
            getData: function () {
                var data = {
                    'method'         : this.getCode(),
                    'additional_data': {
                        firstname           : $('[name="payment[firstname]"]').val(),
                        lastname            : $('[name="payment[lastname]"]').val(),
                        cuit                : $('[name="payment[cuit]"]').val(),
                        email               : $('[name="payment[email]"]').val(),
                        cbu                 : $('[name="payment[cbu]"]').val(),
                        payment_installments: $('[name="payment[payment_installments]"]').val()
                    }
                };

                return data;
            }
        });
    }
);
