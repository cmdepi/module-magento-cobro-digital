/**
 *
 * @description CobroDigital Payment Methods KnockoutJS definition
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */

/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';

        rendererList.push(
            {
                type     : 'cdticket',
                component: 'Cmdepi_CobroDigital/js/view/payment/method-renderer/cdticket-method'
            },
            {
                type     : 'cdbanktransfer',
                component: 'Cmdepi_CobroDigital/js/view/payment/method-renderer/cdbanktransfer-method'
            }
        );

        /**
         *
         * @note Add view logic here if needed
         *
         */
        return Component.extend({});
    }
);
