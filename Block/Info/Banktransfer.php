<?php
/**
 *
 * @description CobroDigital Bank Transfer Payment Method Info Block
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Block\Info;

class Banktransfer extends \Magento\Payment\Block\Info
{
    /**
     *
     * @var string
     *
     */
    protected $_template = 'info/banktransfer.phtml';

    /**
     *
     * Get bank transfer data
     *
     * @return array
     *
     */
    public function getBankTransferData()
    {
        $info = $this->getInfo()->getAdditionalInformation();

        /**
         *
         * @note Unset method title because we use in an other place
         *
         */
        unset($info['method_title']);

        return $info;
    }
}
