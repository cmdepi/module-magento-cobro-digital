<?php
/**
 *
 * @description CobroDigital Payment Ticket Model
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Cd;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Payment\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Cmdepi\CobroDigital\Model\PayerFactory;
use Cmdepi\CobroDigital\Model\Payer;
use Cmdepi\CobroDigital\Model\PaymentTicketFactory;
use Cmdepi\CobroDigital\Model\PaymentTicket;
use Cmdepi\CobroDigital\Model\Payer\AttributeFactory;
use Cmdepi\CobroDigital\Model\Payer\Attribute;

class Ticket extends \Magento\Payment\Model\Method\AbstractMethod
{
    /**
     *
     * Payment method code (use in system.xml -> system -> section 'payment')
     *
     * @const CODE
     *
     */
    const CODE = 'cdticket';

    /**
     *
     * Payment method code
     *
     * @var string
     *
     */
    protected $_code = self::CODE;

    /**
     *
     * Form block path
     *
     * @var string
     *
     */
    protected $_formBlockType = \Cmdepi\CobroDigital\Block\Form\Ticket::class;

    /**
     *
     * Instructions block path
     *
     * @var string
     *
     */
    protected $_infoBlockType = \Cmdepi\CobroDigital\Block\Info\Ticket::class;

    /**
     *
     * Set is offline payment method to true
     *
     * @var bool
     *
     */
    protected $_isOffline = true;

    /**
     *
     * Payer Model Factory
     *
     * @var PayerFactory
     *
     */
    protected $_payerFactory;

    /**
     *
     * Payment Ticket Model Factory
     *
     * @var PaymentTicketFactory
     *
     */
    protected $_paymentTicketFactory;

    /**
     *
     * @var AttributeFactory
     *
     */
    protected $_payerAttributeFactory;

    /**
     *
     * Payer Model
     *
     * @var Payer
     *
     */
    protected $_payer = null;

    /**
     *
     * Payment Ticket Model
     *
     * @var PaymentTicket
     *
     */
    protected $_paymentTicket = null;

    /**
     *
     * @var Attribute
     *
     */
    protected $_payerAttribute = null;

    /**
     *
     * Constructor
     *
     * @param PayerFactory $payerFactory
     * @param PaymentTicketFactory $paymentTicketFactory
     * @param AttributeFactory $payerAttributeFactory
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     */
    public function __construct(
        PayerFactory $payerFactory,
        PaymentTicketFactory $paymentTicketFactory,
        AttributeFactory $payerAttributeFactory,
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        /**
         *
         * Set Payer Factory Model
         *
         */
        $this->_payerFactory = $payerFactory;

        /**
         *
         * Set Payment Ticket Factory Model
         *
         */
        $this->_paymentTicketFactory = $paymentTicketFactory;

        /**
         *
         * Set Payer Attribute Factory Model
         *
         */
        $this->_payerAttributeFactory = $payerAttributeFactory;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     *
     * Assign data to info model instance
     *
     * @param array|\Magento\Framework\DataObject $data
     *
     * @return \Magento\Payment\Model\Method\AbstractMethod
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @api
     *
     * @deprecated 100.2.0
     *
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        /**
         *
         * @note Get payment instance model and quote instance model
         *
         */
        /** @var \Magento\Quote\Model\Quote\Payment $payment */
        $payment = $this->getInfoInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $payment->getQuote();

        /**
         *
         * @note Get payer data to send in 'additional_data'
         *
         */
        $payerAttribute = $this->_getPayerAttribute();
        $payerData      = $data->getData('additional_data');

        /**
         *
         * @note Create Payer (In Magento Platform & CobroDigital platform)
         *
         */
        $payer = $this->_getPayer();

        /**
         *
         * @note Convert payer data because it is using a Magento format. We have to convert it to use CobroDigital needed format to interact with its API
         *
         */
        $information = $payerAttribute->convertToCdData($payerData);
        $payer->setInformation($information);
        if ($customerId = $quote->getCustomer()->getId()) {
            $payer->setCustomerId($customerId);
        }
        $payer->save();

        /**
         *
         * Create Payment Ticket
         *
         * @note See \Cmdepi\CobroDigital\Model\ResourceModel\PaymentTicket->_beforeSave()
         *
         */
        $paymentTicket = $this->_getPaymentTicket();
        $paymentTicket->setPayerId($payer->getId());
        $paymentTicket->setAmount($quote->getGrandTotal());
        $paymentTicket->save();

        /**
         *
         * @note Set payer id and payment ticket id related to the order
         *
         */
        $payment->setAdditionalInformation('payer_id', $payer->getId());
        $payment->setAdditionalInformation('payment_ticket_id', $paymentTicket->getId());

        return parent::assignData($data);
    }

    /**
     *
     * Get Payer Model
     *
     * @return Payer
     *
     */
    protected function _getPayer()
    {
        if (is_null($this->_payer)) {
            $this->_payer = $this->_payerFactory->create();
        }

        return $this->_payer;
    }

    /**
     *
     * Get Payment Ticket Model
     *
     * @return PaymentTicket
     *
     */
    protected function _getPaymentTicket()
    {
        if (is_null($this->_paymentTicket)) {
            $this->_paymentTicket = $this->_paymentTicketFactory->create();
        }

        return $this->_paymentTicket;
    }

    /**
     *
     * Get Payer Attribute
     *
     * @return Attribute
     *
     */
    protected function _getPayerAttribute()
    {
        if (is_null($this->_payerAttribute)) {
            $this->_payerAttribute = $this->_payerAttributeFactory->create();
        }

        return $this->_payerAttribute;
    }
}
