<?php
/**
 *
 * @description CobroDigital Bank Transfer Payment Installments Method
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Config\Source\Banktransfer;

class Paymentinstallmentsmethod implements \Magento\Framework\Option\ArrayInterface
{
    /**
     *
     * @const SEMANA
     *
     */
    const SEMANA = 'semana';

    /**
     *
     * @const SEMANAL
     *
     */
    const SEMANAL = 'semanal';

    /**
     *
     * @const SEMANALES
     *
     */
    const SEMANALES = 'semanales';

    /**
     *
     * @const QUINCENA
     *
     */
    const QUINCENA = 'quincena';

    /**
     *
     * @const QUINCENAL
     *
     */
    const QUINCENAL = 'quincenal';

    /**
     *
     * @const QUINCENALES
     *
     */
    const QUINCENALES = 'quincenales';

    /**
     *
     * @const MES
     *
     */
    const MES = 'mes';

    /**
     *
     * @const MENSUAL
     *
     */
    const MENSUAL = 'mensual';

    /**
     *
     * @const MENSUALES
     *
     */
    const MENSUALES = 'mensuales';

    /**
     *
     * Payment Installment Methods
     *
     * @var array
     *
     */
    protected $_paymentInstallmentsMethods = [
        self::SEMANA,
        self::SEMANAL,
        self::SEMANALES,
        self::QUINCENA,
        self::QUINCENAL,
        self::QUINCENALES,
        self::MES,
        self::MENSUAL,
        self::MENSUALES
    ];

    /**
     *
     * CobroDigital Payer Attributes Options
     *
     * @return array
     *
     */
    public function toOptionArray()
    {
        $options   = array();
        $options[] = array('value' => '', 'label' => __('-- Please Select --'));

        foreach ($this->_paymentInstallmentsMethods as $method) {
            $options[] = array('value' => $method, 'label' => $method);
        }

        return $options;
    }
}