<?php
/**
 *
 * @description CobroDigital Bank Transfer Payment Method Form Block
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Block\Form;

class Banktransfer extends \Magento\Payment\Block\Form
{
    /**
     *
     * @var string
     *
     */
    protected $_template = 'form/banktransfer.phtml';

    /**
     *
     * Get bank transfer data
     *
     * @param string $data
     *
     * @return null|string
     *
     */
    public function getBankTransferData($key)
    {
        /** @var \Magento\Sales\Model\Order\Payment $info */
        $info = $this->getMethod()->getInfoInstance();
        return $info->getAdditionalInformation($key);
    }
}
