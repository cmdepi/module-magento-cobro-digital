<?php
/**
 *
 * @description Payer Model (model to work with 'cd_payer' table)
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Cmdepi\CobroDigital\Model\Payer\AttributeFactory;
use Cmdepi\CobroDigital\Model\Payer\Attribute;

class Payer extends \Magento\Framework\Model\AbstractModel implements \Cmdepi\CobroDigital\Api\Data\PayerInterface
{
    /**
     *
     * @var AttributeFactory
     *
     */
    protected $_payerAttributeFactory;

    /**
     *
     * @var Attribute
     *
     */
    protected $_payerAttribute = null;

    /**
     *
     * Constructor
     *
     * @param AttributeFactory $payerAttributeFactory
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     *
     */
    public function __construct(
        AttributeFactory $payerAttributeFactory,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->_payerAttributeFactory = $payerAttributeFactory;

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     *
     * Initialize model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init(\Cmdepi\CobroDigital\Model\ResourceModel\Payer::class);
    }

    /**
     *
     * Get payer id
     *
     * @return int|null
     *
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     *
     * Get CobroDigital identifier attribute value
     *
     * @return int|null
     *
     * @note It does not have a set method because it is calculated before save action
     *
     */
    public function getIdentifier()
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     *
     * Get payer information
     *
     * @return array
     *
     */
    public function getInformation()
    {
        return $this->getData(self::INFORMATION);
    }

    /**
     *
     * Set payer id
     *
     * @param int $id
     *
     * @return $this
     *
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     *
     * Set payer information. CobroDigital Payer data (see CobroDigital payer structure)
     *
     * @param array $information
     *
     * @return $this
     *
     * @note It is going to have an array with CobroDigital payer structure attribute values. It could have or could not have the payer identifier value. It is not necessary but has to have all other payer structure attributes values. See \Cmdepi\CobroDigital\Model\ResourceModel\Payer->_beforeSave()
     *
     */
    public function setInformation(array $information)
    {
        return $this->setData(self::INFORMATION, $information);
    }

    /**
     *
     * Get payer identifier attribute name/code (CobroDigital ID)
     *
     * @return string
     *
     */
    public function getIdentifierAttribute()
    {
        return $this->_getPayerAttribute()->getIdAttribtue();
    }

    /**
     *
     * Get payer attribute
     *
     * @return Attribute
     *
     */
    protected function _getPayerAttribute()
    {
        if (is_null($this->_payerAttribute)) {
            $this->_payerAttribute = $this->_payerAttributeFactory->create();
        }

        return $this->_payerAttribute;
    }
}
