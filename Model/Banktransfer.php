<?php
/**
 *
 * @description Resource model to work with 'cd_payer' table
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Exception\LocalizedException;
use Exception;
use Cmdepi\CobroDigital\Model\CdFactory;

class Banktransfer
{
    /**
     *
     * System Config Group ID
     *
     * @const BANKTRANSFER_CONFIG_GROUP_ID
     *
     */
    const BANKTRANSFER_CONFIG_GROUP_ID = 'cdbanktransfer';

    /**
     *
     * Bank Transfer concept sended to CobroDigital
     *
     * @const DEFAULT_CONCEPT
     *
     */
    const DEFAULT_CONCEPT = 'Magento Bank Transfer';

    /**
     *
     * CobroDigital Model Factory
     *
     * @var CdFactory
     *
     */
    protected $_cdFactory;

    /**
     *
     * CobroDigital Model
     *
     * @var Cd
     *
     */
    protected $_cd = null;

    /**
     *
     * @var DateTime
     *
     */
    protected $_date;

    /**
     *
     * Constructor
     *
     * @param CdFactory $cdFactory
     * @param DateTime $date
     *
     */
    public function __construct(
        CdFactory $cdFactory,
        DateTime $date
    )
    {
        /**
         *
         * Set CobroDigital factory model to interact with CobroDigital API
         *
         */
        $this->_cdFactory = $cdFactory;

        /**
         *
         * Get date
         *
         */
        $this->_date = $date;
    }

    /**
     *
     * Bank transfer method
     *
     * @param string $firstname Payer firstname
     * @param string $lastname Payer lastname
     * @param string $cuit Payer document (Argentinian DNI, CUIL or CUIT)
     * @param string $email Payer email
     * @param string $cbu Argentinian payer CBU
     * @param int $amount Amount
     * @param int $paymentInstallments Payment installments
     *
     * @return boolean TRUE if process runs ok else FALSE
     *
     * @throws LocalizedException If transfer operations does not run ok, thrown an error (also see $this->_getCd()->bankTransfer(..) & $this->_getCd()->_validateResponse(..))
     * @throws Exception API connection error
     *
     */
    public function transfer($firstname, $lastname, $cuit, $email, $cbu, $amount, $paymentInstallments)
    {
        $success = $this->_getCd()->bankTransfer($firstname, $lastname, $cuit, $email, $cbu, $amount, $this->_getDaysToTransfer(), self::DEFAULT_CONCEPT, $paymentInstallments, $this->_getPaymentInstallmentsMethod());

        if (!$success) {
            throw new LocalizedException(__('Bank Transfer Error'));
        }

        return $success;
    }

    /**
     *
     * Get Payment Installments
     *
     * @return array
     *
     */
    public function getPaymentInstallments()
    {
        return explode(',', $this->_getCd()->getConfigData(self::BANKTRANSFER_CONFIG_GROUP_ID, 'payment_installments'));
    }

    /**
     *
     * Get days when bank transfer is applied
     *
     * @return string
     *
     */
    protected function _getDaysToTransfer()
    {
        $days                   = $this->_getCd()->getConfigData(self::BANKTRANSFER_CONFIG_GROUP_ID, 'days_to_transfer');
        $date                   = $this->_date->date('Ymd');
        $transferDaysExpression = sprintf('%s + %s days', $date, $days);
        $transferTime           = strtotime($transferDaysExpression);

        return $this->_date->date('Ymd', $transferTime);
    }

    /**
     *
     * Get Payment Installments Method
     *
     * @return string
     *
     */
    protected function _getPaymentInstallmentsMethod()
    {
        return $this->_getCd()->getConfigData(self::BANKTRANSFER_CONFIG_GROUP_ID, 'payment_installments_method');
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return Cd
     *
     */
    protected function _getCd()
    {
        if (is_null($this->_cd)) {
            $this->_cd = $this->_cdFactory->create();
        }

        return $this->_cd;
    }
}