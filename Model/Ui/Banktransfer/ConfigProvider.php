<?php
/**
 *
 * @description CobroDigital Bank Transfer Payment Method Config Provider to send data to Knockout JS view/frontend/web/js/view/payment/method-renderer/cdbanktransfer-method.js
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Ui\Banktransfer;

use Magento\Checkout\Model\ConfigProviderInterface;
use Cmdepi\CobroDigital\Model\Cd\Banktransfer;
use Cmdepi\CobroDigital\Model\BanktransferFactory as BanktransferFactoryModel;
use Cmdepi\CobroDigital\Model\Banktransfer        as BanktransferModel;

final class ConfigProvider implements ConfigProviderInterface
{
    /**
     *
     * Bank Transfer Factory Model
     *
     * @var BanktransferFactoryModel
     *
     */
    protected $_bankTransferFactory;

    /**
     *
     * Bank Transfer Model
     *
     * @var BanktransferModel
     *
     */
    protected $_bankTransfer = null;

    /**
     *
     * Constructor
     *
     * @param BanktransferFactoryModel $bankTransferFactory
     *
     */
    public function __construct(
        BanktransferFactoryModel $bankTransferFactory
    ) {
        /**
         *
         * Set Bank Transfer Factory Model
         *
         */
        $this->_bankTransferFactory = $bankTransferFactory;
    }

    /**
     *
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     *
     */
    public function getConfig()
    {
        return [
            'payment' => [
                Banktransfer::CODE => [
                    'paymentinstallments' => $this->_getPaymentInstallments()
                ]
            ]
        ];
    }

    /**
     *
     * Get payment installments formatted to send to knockout js
     *
     * @return array
     *
     */
    protected function _getPaymentInstallments()
    {
        $paymentInstallments = $this->_getBanktransferModel()->getPaymentInstallments();
        $data                = array();

        foreach ($paymentInstallments as $paymentInstallment) {
            $data[] = array('value' => $paymentInstallment, 'label' => $paymentInstallment);
        }

        return $data;
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return BanktransferModel
     *
     */
    protected function _getBanktransferModel()
    {
        if (is_null($this->_bankTransfer)) {
            $this->_bankTransfer = $this->_bankTransferFactory->create();
        }

        return $this->_bankTransfer;
    }
}