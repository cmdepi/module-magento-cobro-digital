<?php
/**
 *
 * @description CobroDigital Payer Attributes Options
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Config\Source\Payer;

use Exception;
use Cmdepi\CobroDigital\Model\Payer\AttributeFactory as PayerAttributeFactory;
use Cmdepi\CobroDigital\Model\Payer\Attribute        as PayerAttribute;

class Attribute implements \Magento\Framework\Option\ArrayInterface
{
    /**
     *
     * @var PayerAttributeFactory
     *
     */
    protected $_payerAttributeFactory;

    /**
     *
     * @var PayerAttribute
     *
     */
    protected $_payerAttribute = null;

    /**
     *
     * Constructor
     *
     * @param PayerAttributeFactory $payerAttributeFactory
     *
     */
    public function __construct(
        PayerAttributeFactory $payerAttributeFactory
    ) {
        $this->_payerAttributeFactory = $payerAttributeFactory;
    }


    /**
     *
     * CobroDigital Payer Attributes Options
     *
     * @return array
     *
     */
    public function toOptionArray()
    {
        $options   = array();
        $options[] = array('value' => '', 'label' => __('-- Please Select --'));

        /**
         *
         * @note Get payer attributes from CobroDigital system
         *
         */
        try{
            $attributes = $this->_getPayerAttribute()->getPayerStructure();
            foreach ($attributes as $attribute) {
                $options[] = array('value' => $attribute, 'label' => $attribute);
            }
        }
        catch (Exception $e) {}

        return $options;
    }

    /**
     *
     * Get payer attribute
     *
     * @return PayerAttribute
     *
     */
    protected function _getPayerAttribute()
    {
        if (is_null($this->_payerAttribute)) {
            $this->_payerAttribute = $this->_payerAttributeFactory->create();
        }

        return $this->_payerAttribute;
    }
}