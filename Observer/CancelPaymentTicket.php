<?php
/**
 *
 * @description Observer to cancel payment ticket when an order is canceled
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\ValidatorException;
use Exception;
use Cmdepi\CobroDigital\Model\Sales\Order;
use Cmdepi\CobroDigital\Model\Cd\Ticket;

class CancelPaymentTicket implements ObserverInterface
{
    /**
     *
     * Cancel order payment ticket when order is canceled
     *
     * @param Observer $observer
     *
     * @return void
     *
     * @throws ValidatorException
     *
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();

        try {
            /**
             *
             * @note If order is a Payment Ticket Order
             *
             */
            if ($order->getPayment()->getMethod() == Ticket::CODE) {
                $paymentTicket = $order->getPaymentTicket();
                if ($paymentTicket) {
                    $paymentTicket->cancel();
                }
            }
        }
        catch(Exception $e) {
            throw new ValidatorException(__($e->getMessage()));
        }
    }
}