<?php
/**
 *
 * @description Collection model to work with 'cd_payer' table
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\ResourceModel\Payer;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * Initialize collection model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init(\Cmdepi\CobroDigital\Model\Payer::class, \Cmdepi\CobroDigital\Model\ResourceModel\Payer::class);
    }
}