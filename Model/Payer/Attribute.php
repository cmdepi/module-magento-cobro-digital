<?php
/**
 *
 * @description Resource model to work with 'cd_payer' table
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Payer;

use Cmdepi\CobroDigital\Model\CdFactory;
use Cmdepi\CobroDigital\Model\Cd;
use Cmdepi\CobroDigital\Helper\Payer\Attribute as AttributeHelper;

class Attribute
{
    /**
     *
     * CobroDigital Model Factory
     *
     * @var CdFactory
     *
     */
    protected $_cdFactory;

    /**
     *
     * CobroDigital Attribute Helper
     *
     * @var AttributeHelper
     *
     */
    protected $_attributeHelper;

    /**
     *
     * CobroDigital Model
     *
     * @var Cd
     *
     */
    protected $_cd = null;

    /**
     *
     * Constructor
     *
     * @param CdFactory $cdFactory
     * @param AttributeHelper $attributeHelper
     *
     */
    public function __construct(
        CdFactory $cdFactory,
        AttributeHelper $attributeHelper
    )
    {
        /**
         *
         * Set CobroDigital factory model to interact with CobroDigital API
         *
         */
        $this->_cdFactory = $cdFactory;

        /**
         *
         * Set CobroDigital attribute helper
         *
         */
        $this->_attributeHelper = $attributeHelper;
    }

    /**
     *
     * Convert an array with Magento keys to an array with CobroDigital keys available to interact with its API
     *
     * @param array $payerData Array with payer data values using Magento keys (see $this->_attributeHelper->getMagentoKeyFromCobroDigitalKey($cdkey))
     *
     * @return array
     *
     */
    public function convertToCdData($payerData)
    {
        $data       = array();
        $attributes = $this->getMappedAttributes();

        /**
         *
         * @note Convert external (visible) attributes configured to be seen in payment form
         *
         */
        foreach ($payerData as $key => $value) {
            foreach ($attributes as $attribute) {
                if ($attribute['magekey'] == $key) {
                    $data[$attribute['cdkey']] = $value;
                }
            }
        }

        /**
         *
         * Set CobroDigital Payer ID data
         *
         * @note We have to set as an extra value because it is an internal attribute
         *
         */
        if (isset($payerData[$this->getIdAttribtue()])) {
            $data[$this->getIdAttribtue()] = $payerData[$this->getIdAttribtue()];
        }

        return $data;
    }

    /**
     *
     * Get payer attributes.
     *
     * @return array
     *
     * @note Parse CobroDigital response to be correct to use
     *
     */
    public function getPayerStructure()
    {
        $payerStructure = $this->_getCd()->getPayerStructure();
        $attributes     = [];

        foreach ($payerStructure as $payerAttribute) {
            $attributes[] = $this->_parseAttribute($payerAttribute);
        }

        return $attributes;
    }

    /**
     *
     * Get payer attributes mapped with its respective label and Magento Key.
     *
     * @return array
     *
     * @note If the payer attribute does not appear in payment/cd/map_attribute_label, it is not used.
     *
     */
    public function getMappedAttributes()
    {
        $attributes = $this->_attributeHelper->getCobroDigitalFormLabelsMapper($this->_getCd()->getAttributesLabelsMap());

        foreach ($attributes as &$attribute) {
            $mageKey              = $this->_attributeHelper->getMagentoKeyFromCobroDigitalKey($attribute['cdkey']);
            $attribute['magekey'] = $mageKey;
        }

        return $attributes;
    }

    /**
     *
     * Get ID attribute code/name.
     *
     */
    public function getIdAttribtue()
    {
        return $this->_getCd()->getPayerIdentifierAttribute();
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return Cd
     *
     */
    protected function _getCd()
    {
        if (is_null($this->_cd)) {
            $this->_cd = $this->_cdFactory->create();
        }

        return $this->_cd;
    }

    /**
     *
     * Parse Attribute
     *
     * @param string $attribute
     *
     * @return string
     *
     * @note CobroDigital attributes has double&double quotes
     *
     */
    private function _parseAttribute($attribute)
    {
        /**
         *
         * @note Remove first and last double quotes
         *
         */
        $start  = 1;
        $length = strlen($attribute) - 2;
        return substr($attribute, $start, $length);
    }
}