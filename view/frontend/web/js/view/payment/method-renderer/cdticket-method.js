/**
 *
 * @description CobroDigital Payment Ticket Method KnockoutJS Logic
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
define(
    [
        'ko',
        'Magento_Checkout/js/view/payment/default',
        'jquery'
    ],
    function (ko, Component, $) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Cmdepi_CobroDigital/payment/cdticket'
            },

            /**
             *
             * Get due date text message
             *
             */
            getDueDateText: function () {
                return window.checkoutConfig.payment.cdticket.duedatetext;
            },

            /**
             *
             * Get form HTML
             *
             */
            getFormHtml: function () {
                return window.checkoutConfig.payment.cdticket.formhtml;
            },

            /**
             *
             * Get payer attributes
             *
             * @return {object}
             *
             */
            getPayerAttributes: function () {
                return window.checkoutConfig.payment.cdticket.payerattributes;
            },

            /**
             *
             * Data to return when place order happened
             *
             * @returns {{method: (*|String), additional_data: {payment_method_nonce: (paymentMethodNonce|String)}}}
             *
             * @note Send data to
             *
             */
            getData: function () {
                var additionalData = {};
                var attributes     = this.getPayerAttributes();

                for (var key in attributes) {
                    var value = $('[name="' + attributes[key]['magekey'] + '"]').val();
                    additionalData[attributes[key]['magekey']] = value;
                }

                var data = {
                    'method'         : this.getCode(),
                    'additional_data': additionalData
                };

                return data;
            }
        });
    }
);
