<?php
/**
 *
 * @description CobroDigital Payer Helper
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Helper;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Helper\Context;

class Payer extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     * Payer identifier value prefix
     *
     * @const ID_PREFIX
     *
     */
    const ID_PREFIX = 'CD';

    /**
     *
     * Payer identifier value suffix
     *
     * @const ID_SUFFIX
     *
     */
    const ID_SUFFIX = 'MAGE';

    /**
     *
     * @var DateTime
     *
     */
    protected $_date;

    /**
     *
     * Constructor
     *
     * @param DateTime $date
     * @param Context $context
     *
     */
    public function __construct(
        DateTime $date,
        Context $context
    )
    {
        /**
         *
         * Get date
         *
         */
        $this->_date = $date;

        parent::__construct($context);
    }

    /**
     *
     * Create payer identifier value to CobroDigital system
     *
     * @return string
     *
     * @todo Replace this feature using a sequence pattern like increment id in order model
     *
     */
    public function createIdentifier()
    {
        return sprintf('%s-%s-%s', self::ID_PREFIX, $this->_date->gmtDate('YmdHis'), self::ID_SUFFIX);
    }
}
