<?php
/**
 *
 * @description CobroDigital Payment Method Upgrade Schema Feature
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     *
     * Upgrade schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /**
         *
         * If the version of the module is lower than '0.3.0', run this upgrade
         *
         */
        if (version_compare($context->getVersion(), '0.3.0') < 0) {
            $this->_upgradeVersionZeroThreeZero($setup);
        }

        /**
         *
         * If the version of the module is lower than '0.4.0', run this upgrade
         *
         */
        if (version_compare($context->getVersion(), '0.4.0') < 0) {
            $this->_upgradeVersionZeroFourZero($setup);
        }

        $setup->endSetup();
    }

    /**
     *
     * Add custom tables to save payer data and payment ticket data
     *
     * @param SchemaSetupInterface $setup
     *
     */
    protected function _upgradeVersionZeroThreeZero(SchemaSetupInterface $setup)
    {
        /**
         *
         * Create 'cd_payer' table to save payer data
         *
         * @note It is necessary this table because CobroDigital does not have a resource in its API that allow gets payer data
         * @note We are going to save the payer data in 'data' column serialized. We are going to use this mode to save this data because the payer attributes could change over time
         *
         */
        $payerTable = $setup->getConnection()->newTable(
            $setup->getTable('cd_payer')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Payer ID'
        )->addColumn(
            'identifier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            ['nullable' => false],
            'CobroDigital Payer Identifier'
        )->addColumn(
            'information',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            [],
            'Payer Data'
        )->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'unsigned' => true],
            'Customer ID'
        )->addForeignKey(
            $setup->getFkName(
                'cd_payer',
                'customer_id',
                'customer_entity',
                'customer_id'
            ),
            'customer_id',
            $setup->getTable('customer_entity'),
            'entity_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Payer'
        );
        $setup->getConnection()->createTable($payerTable);

        /**
         *
         * Create 'cd_payment_ticket' table to save payment ticket data
         *
         * @note It is necessary this table because CobroDigital does not have a resource in its API that allow gets payment ticket data
         *
         */
        $paymentTickettable = $setup->getConnection()->newTable(
            $setup->getTable('cd_payment_ticket')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Payment Ticket ID'
        )->addColumn(
            'nro_boleta',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'CobroDigital Payment Ticket Identifier'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => false],
            'CobroDigital Created At'
        )->addColumn(
            'due_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => false],
            'CobroDigital Due Data'
        )->addColumn(
            'amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['default' => '0.0000'],
            'Amount'
        )->addColumn(
            'payer_id',\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'unsigned' => true],
            'Payer ID'
        )->addForeignKey(
            $setup->getFkName(
                'cd_payment_ticket',
                'payer_id',
                'cd_payer',
                'entity_id'
            ),
            'payer_id',
            $setup->getTable('cd_payer'),
            'entity_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Payment Ticket'
        );

        $setup->getConnection()->createTable($paymentTickettable);
    }

    /**
     *
     * Update 'cd_payer' table to remove 'customer_id' column because of CobroDigital API not allow buy multiple times in a same date by the same payer, so now, we are going to have a new payer for each order and the relationship between payer and customer is going to disappear until this problem is solved
     *
     * @param SchemaSetupInterface $setup
     *
     */
    protected function _upgradeVersionZeroFourZero(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->dropColumn($setup->getTable('cd_payer'), 'customer_id');
        $setup->getConnection()->dropForeignKey(
            'cd_payer',
            'CD_PAYER_CUSTOMER_ID_CUSTOMER_ENTITY_CUSTOMER_ID'
        );
    }
}