<?php
/**
 *
 * @description CobroDigital Ticket Payment Method Config Provider to send data to Knockout JS view/frontend/web/js/view/payment/method-renderer/cdticket-method.js
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Ui\Ticket;

use Magento\Checkout\Model\ConfigProviderInterface;
use Cmdepi\CobroDigital\Model\CdFactory;
use Cmdepi\CobroDigital\Model\Cd;
use Cmdepi\CobroDigital\Model\Cd\Ticket;
use Cmdepi\CobroDigital\Model\Payer\AttributeFactory;
use Cmdepi\CobroDigital\Model\Payer\Attribute;
use Cmdepi\CobroDigital\Model\PaymentTicket as PaymentTicketModel;

final class ConfigProvider implements ConfigProviderInterface
{
    /**
     *
     * CobroDigital Model
     *
     * @var CdFactory
     *
     */
    protected $_cdFactory;

    /**
     *
     * @var AttributeFactory
     *
     */
    protected $_payerAttributeFactory;

    /**
     *
     * @var Attribute
     *
     */
    protected $_payerAttribute = null;

    /**
     *
     * CobroDigital Model
     *
     * @var Cd
     *
     */
    protected $_cd = null;

    /**
     *
     * Constructor
     *
     * @param CdFactory $cdFactory
     * @param AttributeFactory $payerAttributeFactory
     *
     */
    public function __construct(
        CdFactory $cdFactory,
        AttributeFactory $payerAttributeFactory
    ) {
        /**
         *
         * Set CobroDigital factory model to interact with CobroDigital API
         *
         */
        $this->_cdFactory = $cdFactory;

        /**
         *
         * Set Payer Attribute Factory Model
         *
         */
        $this->_payerAttributeFactory = $payerAttributeFactory;
    }

    /**
     *
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     *
     */
    public function getConfig()
    {
        return [
            'payment' => [
                Ticket::CODE => [
                    'formhtml'        => $this->_getFormHtml(),
                    'payerattributes' => $this->_getPayerMappedAttributes(),
                    'duedatetext'     => $this->_getDueDateText()
                ]
            ]
        ];
    }

    /**
     *
     * Get due date text
     *
     * @return int
     *
     */
    protected function _getDueDateText()
    {
        $days = $this->_getCd()->getConfigData(PaymentTicketModel::PAYMENT_TICKET_CONFIG_GROUP_ID, 'days_to_expiration');
        return sprintf(__('The payment ticket expires in %s working days'), $days);
    }

    /**
     *
     * Get Payer Mapped Attributes
     *
     * @return array
     *
     */
    protected function _getPayerMappedAttributes()
    {
        return $this->_getPayerAttribute()->getMappedAttributes();
    }

    /**
     *
     * Create dinamically Payment Form using Payer Attributes got from API
     *
     */
    private function _getFormHtml()
    {
        $html       = '';
        $attributes = $this->_getPayerMappedAttributes();

        /**
         *
         * @note Create Payer Data form with visible attributes
         *
         */
        foreach ($attributes as $attribute) {
            $html .= '<div class="field required">';
            $html .= '<label class="label">' . $attribute['label'] . '</label>';
            $html .= '<div class="control">';
            $html .= '<input type="text" name="' . $attribute['magekey'] . '" class="input-text required-entry"/>';
            $html .= '</div>';
            $html .= '</div>';
        }

        return $html;
    }

    /**
     *
     * Get Payer Attribute
     *
     * @return Attribute
     *
     */
    protected function _getPayerAttribute()
    {
        if (is_null($this->_payerAttribute)) {
            $this->_payerAttribute = $this->_payerAttributeFactory->create();
        }

        return $this->_payerAttribute;
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return Cd
     *
     */
    protected function _getCd()
    {
        if (is_null($this->_cd)) {
            $this->_cd = $this->_cdFactory->create();
        }

        return $this->_cd;
    }
}