<?php
/**
 *
 * @description CobroDigital Payer Attribute Helper
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Helper\Payer;

use Magento\Framework\App\Helper\Context;

class Attribute extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     * Constructor
     *
     * @param Context $context
     *
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     *
     * Get array that map CobroDigital attributes with payment form labels
     *
     * @param string $map An string with the format: CobroDigital Attribute1:Form Label1,CobroDigital Attribute2:Form Label2,... (See payment/cd/map_attribute_label)
     *
     * @return array Array to map CobroDigital attributes with payment form labels. Format: ('cdkey' => 'value', 'label' => 'value', ...)
     *
     */
    public function getCobroDigitalFormLabelsMapper($map)
    {
        $mapped         = array();
        $attributeLabel = explode(',', $map);

        foreach ($attributeLabel as $attLab) {
            list($att, $lab) = explode(':', $attLab);
            $mapped[] = array('cdkey' => $att, 'label' => $lab);
        }

        return $mapped;
    }

    /**
     *
     * Because CobroDigital keys has spaces, and this types of keys can not be sent by a form, It was neccesary to generate our keys to map with CobroDigital keys later
     *
     * @param string $cdkey CobroDigital key
     *
     * @return string Magento key
     *
     */
    public function getMagentoKeyFromCobroDigitalKey($cdkey)
    {
        return implode('_', explode(' ', $cdkey));
    }
}
