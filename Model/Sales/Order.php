<?php
/**
 *
 * @description Customization to order model
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Sales;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Sales\Model\Order\Config as OrderConfig;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory as OrderItemCollectionFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory        as OrderAddressCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory        as OrderPaymentCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory as OrderStatusHistoryCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory        as OrderInvoiceCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory       as OrderShipmentCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory     as OrderCreditmemoCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory as OrderShipmentTrackCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory                as OrderCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory            as ProductCollection;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Cmdepi\CobroDigital\Model\PayerFactory;
use Cmdepi\CobroDigital\Model\Payer;
use Cmdepi\CobroDigital\Model\PaymentTicketFactory;
use Cmdepi\CobroDigital\Model\PaymentTicket;

class Order extends \Magento\Sales\Model\Order
{
    /**
     *
     * Payer Model Factory
     *
     * @var PayerFactory
     *
     */
    protected $_payerFactory;

    /**
     *
     * Payment Ticket Model Factory
     *
     * @var PaymentTicketFactory
     *
     */
    protected $_paymentTicketFactory;

    /**
     *
     * Payer Model
     *
     * @var Payer
     *
     */
    protected $_payer = null;

    /**
     *
     * Payment Ticket Model
     *
     * @var PaymentTicket
     *
     */
    protected $_paymentTicket = null;

    /**
     *
     * Constructor
     *
     * @param PayerFactory $payerFactory
     * @param PaymentTicketFactory $paymentTicketFactory
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param TimezoneInterface $timezone
     * @param StoreManagerInterface $storeManager
     * @param OrderConfig $orderConfig
     * @param ProductRepositoryInterface $productRepository
     * @param OrderItemCollectionFactory $orderItemCollectionFactory
     * @param Visibility $productVisibility
     * @param InvoiceManagementInterface $invoiceManagement
     * @param CurrencyFactory $currencyFactory
     * @param EavConfig $eavConfig
     * @param HistoryFactory $orderHistoryFactory
     * @param OrderAddressCollectionFactory $addressCollectionFactory
     * @param OrderPaymentCollectionFactory $paymentCollectionFactory
     * @param OrderStatusHistoryCollectionFactory $historyCollectionFactory
     * @param OrderInvoiceCollectionFactory $invoiceCollectionFactory
     * @param OrderShipmentCollectionFactory $shipmentCollectionFactory
     * @param OrderCreditmemoCollectionFactory $memoCollectionFactory
     * @param OrderShipmentTrackCollectionFactory $trackCollectionFactory
     * @param OrderCollection $salesOrderCollectionFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param ProductCollection $productListFactory
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     */
    public function __construct(
        PayerFactory $payerFactory,
        PaymentTicketFactory $paymentTicketFactory,
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        TimezoneInterface $timezone,
        StoreManagerInterface $storeManager,
        OrderConfig $orderConfig,
        ProductRepositoryInterface $productRepository,
        OrderItemCollectionFactory $orderItemCollectionFactory,
        Visibility $productVisibility,
        InvoiceManagementInterface $invoiceManagement,
        CurrencyFactory $currencyFactory,
        EavConfig $eavConfig,
        HistoryFactory $orderHistoryFactory,
        OrderAddressCollectionFactory $addressCollectionFactory,
        OrderPaymentCollectionFactory $paymentCollectionFactory,
        OrderStatusHistoryCollectionFactory $historyCollectionFactory,
        OrderInvoiceCollectionFactory $invoiceCollectionFactory,
        OrderShipmentCollectionFactory $shipmentCollectionFactory,
        OrderCreditmemoCollectionFactory $memoCollectionFactory,
        OrderShipmentTrackCollectionFactory $trackCollectionFactory,
        OrderCollection $salesOrderCollectionFactory,
        PriceCurrencyInterface $priceCurrency,
        ProductCollection $productListFactory,
        AbstractResource $resource     = null,
        AbstractDb $resourceCollection = null,
        array $data                    = []
    ) {
        /**
         *
         * Set Payer Factory Model
         *
         */
        $this->_payerFactory = $payerFactory;

        /**
         *
         * Set Payment Ticket Factory Model
         *
         */
        $this->_paymentTicketFactory = $paymentTicketFactory;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $timezone,
            $storeManager,
            $orderConfig,
            $productRepository,
            $orderItemCollectionFactory,
            $productVisibility,
            $invoiceManagement,
            $currencyFactory,
            $eavConfig,
            $orderHistoryFactory,
            $addressCollectionFactory,
            $paymentCollectionFactory,
            $historyCollectionFactory,
            $invoiceCollectionFactory,
            $shipmentCollectionFactory,
            $memoCollectionFactory,
            $trackCollectionFactory,
            $salesOrderCollectionFactory,
            $priceCurrency,
            $productListFactory,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     *
     * Get Payer Model related to the order
     *
     * @return null|Payer
     *
     * @todo Improve. Use Repositories.
     *
     */
    public function getPayer()
    {
        $payment = $this->getPayment()->getAdditionalInformation();
        return $this->_getPayer()->load($payment['payer_id']);
    }

    /**
     *
     * Get Payment Ticket Model related to the order
     *
     * @return null|PaymentTicket
     *
     * @todo Improve. Use Repositories.
     *
     */
    public function getPaymentTicket()
    {
        $payment = $this->getPayment()->getAdditionalInformation();

        if (isset($payment['payment_ticket_id'])) {
            return $this->_getPaymentTicket()->load($payment['payment_ticket_id']);
        }

        return null;
    }

    /**
     *
     * Get Payer Model
     *
     * @return Payer
     *
     */
    protected function _getPayer()
    {
        if (is_null($this->_payer)) {
            $this->_payer = $this->_payerFactory->create();
        }

        return $this->_payer;
    }

    /**
     *
     * Get Payment Ticket Model
     *
     * @return PaymentTicket
     *
     */
    protected function _getPaymentTicket()
    {
        if (is_null($this->_paymentTicket)) {
            $this->_paymentTicket = $this->_paymentTicketFactory->create();
        }

        return $this->_paymentTicket;
    }
}
