<?php
/**
 *
 * @description Module Logger Handler
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Logger;

use Monolog\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     *
     * Logging level
     *
     * @var int
     *
     */
    protected $loggerType = Logger::INFO;

    /**
     *
     * File name
     *
     * @var string
     *
     */
    protected $fileName = '/var/log/cd.log';
}