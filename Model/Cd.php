<?php
/**
 *
 * @description CobroDigital Model
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Cmdepi\CobroDigital\Factory\ApiFactory;
use Cmdepi\CobroDigital\Logger\Logger;

class Cd
{
    /**
     *
     * System Config Group ID
     *
     * @const CD_CONFIG_GROUP_ID
     *
     */
    const CD_CONFIG_GROUP_ID = 'cd';

    /**
     *
     * Default Payment Ticket Template
     *
     * @const CREATE_TICKET_DEFAULT_TEMPLATE
     *
     */
    const TICKET_DEFAULT_TEMPLATE = 'init';

    /**
     *
     * CobroDigital API
     *
     * @var ApiFactory
     *
     */
    protected $_apiFactory;

    /**
     *
     * @var StoreManagerInterface
     *
     */
    protected $_storeManager;

    /**
     *
     * Magento config
     *
     * @var ScopeConfigInterface
     *
     */
    protected $_scopeConfig;

    /**
     *
     * Logger
     *
     * @var Logger
     *
     */
    protected $_logger;

    /**
     *
     * Json helper
     *
     * @var JsonHelper
     *
     */
    protected $_jsonHelper;

    /**
     *
     * Website ID to get config values
     *
     * @var int|string
     *
     */
    private $_websiteId = null;

    /**
     *
     * Constructor
     *
     * @param ApiFactory $apiFactory
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param JsonHelper $jsonHelper
     *
     */
    public function __construct(
        ApiFactory $apiFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        JsonHelper $jsonHelper
    )
    {
        /**
         *
         * Set API factory object to interact with CobroDigital
         *
         */
        $this->_apiFactory = $apiFactory;

        /**
         *
         * @note Set store manager
         *
         */
        $this->_storeManager = $storeManager;

        /**
         *
         * Set Magento config object
         *
         */
        $this->_scopeConfig = $scopeConfig;

        /**
         *
         * Set JSON helper
         *
         */
        $this->_jsonHelper = $jsonHelper;

        /**
         *
         * Set Logger
         *
         */
        $this->_logger = $logger;
    }

    /**
     *
     * Set custom website ID to use on config values scope getter
     *
     * @param int|string $websiteId
     *
     * @return void
     *
     */
    public function setWebsiteId($websiteId)
    {
        $this->_websiteId = $websiteId;
    }

    /**
     *
     * Get payer structure. Get payer attributes.
     *
     * @return array Payer attributes
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function getPayerStructure()
    {
        $this->_debug('-- Get Payer Structure --');
        $this->_debug('- Input -');
        $this->_debug('No Data');

        try {
            $api      = $this->_getApi();
            $response = $api->getPayerStructure();

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);

            /**
             *
             * @note Fix API 'consultar_estructura_pagadores' method. Return 'ejecucion_correcta' = false if runs ok
             *
             */
            if (isset($response['datos']) && count($response['datos'])) {
                $response['ejecucion_correcta'] = true;
            }
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['datos'];
    }

    /**
     *
     * Create Payer
     *
     * @param array $payerData Payer creation data
     *
     * @return boolean TRUE if the process runs ok else FALSE
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function createPayer($payerData)
    {
        $this->_debug('-- Create Payer --');
        $this->_debug('- Input -');
        $this->_debug('Payer Data:');
        $this->_debug($payerData);

        try {
            $api      = $this->_getApi();
            $response = $api->createPayer($payerData);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['ejecucion_correcta'];
    }

    /**
     *
     * Edit Payer
     *
     * @param string $identifierAttribute Identifier attribute name
     * @param string $identifierValue Identifier value
     * @param array $payerData Payer data to update
     *
     * @return boolean TRUE if process runs ok else FALSE
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function editPayer($identifierAttribute, $identifierValue, $payerData)
    {
        $this->_debug('-- Edit Payer --');
        $this->_debug('- Input -');
        $this->_debug('Identifier Attribute:');
        $this->_debug($identifierAttribute);
        $this->_debug('Identifier Value:');
        $this->_debug($identifierValue);
        $this->_debug('Payer Data:');
        $this->_debug($payerData);

        try {
            $api      = $this->_getApi();
            $response = $api->editPayer($identifierAttribute, $identifierValue, $payerData);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['ejecucion_correcta'];
    }

    /**
     *
     * Validate payer existence
     *
     * @param string $identifierAttribute Payer identifier attribute name
     * @param string|int $identifierValue Payer identifier attribute value
     *
     * @return boolean
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function payerExists($identifierAttribute, $identifierValue)
    {
        $this->_debug('-- Payer Exists --');
        $this->_debug('- Input -');
        $this->_debug('Identifier Attribute:');
        $this->_debug($identifierAttribute);
        $this->_debug('Identifier Value:');
        $this->_debug($identifierValue);

        try {
            $api      = $this->_getApi();
            $response = $api->payerExists($identifierAttribute, $identifierValue);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['ejecucion_correcta'];
    }

    /**
     *
     * Create payment ticket
     *
     * @param string $identifierAttribute Payer identifier attribute name
     * @param string|int $identifierValue Payer identifier attribute value
     * @param string $concept Payment ticket reason
     * @param array $dueDates Due dates. Date has to have the format 'YYYYMMDD'
     * @param array $amounts Amounts array
     *
     * @return array Payment Ticket ID ('nro_boleta')
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function createPaymentTicket($identifierAttribute, $identifierValue, $concept, $dueDates, $amounts)
    {
        $this->_debug('-- Create Payment Ticket --');
        $this->_debug('- Input -');
        $this->_debug('Identifier Attribute:');
        $this->_debug($identifierAttribute);
        $this->_debug('Identifier Value');
        $this->_debug($identifierValue);
        $this->_debug('Concept');
        $this->_debug($concept);
        $this->_debug('Due Dates');
        $this->_debug($dueDates);
        $this->_debug('Amounts');
        $this->_debug($amounts);

        try {
            $api      = $this->_getApi();
            $response = $api->createPaymentTicket($identifierAttribute, $identifierValue, $concept, self::TICKET_DEFAULT_TEMPLATE, $dueDates, $amounts);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['datos'][0];
    }

    /**
     *
     * Disable payment ticket
     *
     * @param string $paymentTicketId Payment Ticket ID
     *
     * @return boolean TRUE if process runs ok else FALSE
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function disablePaymentTicket($paymentTicketId)
    {
        $this->_debug('-- Disable Payment Ticket --');
        $this->_debug('- Input -');
        $this->_debug('Payment Ticket ID:');
        $this->_debug($paymentTicketId);

        try {
            $api      = $this->_getApi();
            $response = $api->disablePaymentTicket($paymentTicketId);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['ejecucion_correcta'];
    }

    /**
     *
     * Get payment ticket HTML
     *
     * @param string $paymentTicketId Payment Ticket ID
     *
     * @return string Payment Ticket HTML
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function getPaymentTicketHTML($paymentTicketId)
    {
        $this->_debug('-- Get Payment Ticket HTML --');
        $this->_debug('- Input -');
        $this->_debug('Payment Ticket ID:');
        $this->_debug($paymentTicketId);

        try {
            $api      = $this->_getApi();
            $response = $api->getPaymentTicketHTML($paymentTicketId);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['datos'][0]['boleta'];
    }

    /**
     *
     * Bank transfer method
     *
     * @param string $firstname Payer firstname
     * @param string $lastname Payer lastname
     * @param string $cuit Payer document (Argentinian DNI, CUIL or CUIT)
     * @param string $email Payer email
     * @param string $cbu Argentinian payer CBU
     * @param int $amount Amount
     * @param string $date Date with format 'YYYYMMDD'
     * @param string $concept Reason
     * @param int $paymentInstallments Payment installments
     * @param string $paymentInstallmentsMethod Payment installments method
     *
     * @return boolean TRUE if process runs ok else FALSE
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function bankTransfer($firstname, $lastname, $cuit, $email, $cbu, $amount, $date, $concept, $paymentInstallments = null, $paymentInstallmentsMethod = null)
    {
        $this->_debug('-- Bank Transfer --');
        $this->_debug('- Input -');
        $this->_debug('Name:');
        $this->_debug($firstname);
        $this->_debug('Lastname:');
        $this->_debug($lastname);
        $this->_debug('CUIT:');
        $this->_debug($cuit);
        $this->_debug('Email:');
        $this->_debug($email);
        $this->_debug('CBU:');
        $this->_debug($cbu);
        $this->_debug('Amount:');
        $this->_debug($amount);
        $this->_debug('Date:');
        $this->_debug($date);
        $this->_debug('Concept:');
        $this->_debug($concept);
        $this->_debug('Payment Installments:');
        $this->_debug($paymentInstallments);
        $this->_debug('Payment Installments Method:');
        $this->_debug($paymentInstallmentsMethod);

        try {
            $api      = $this->_getApi();
            $response = $api->bankTransfer($firstname, $lastname, $cuit, $email, $cbu, $amount, $date, $concept, $paymentInstallments, $paymentInstallmentsMethod);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['ejecucion_correcta'];
    }

    /**
     *
     * Get successful transactions
     *
     * @param string $from Date from. Date with the form 'Ymd'
     * @param string $to Date to. Date with the form 'Ymd'
     * @param array $filters Array to filter result. Array with the form ('nro_boleta' => 'valor1', 'concepto' => 'valor2', 'identificador' => 'valor3', 'nombre' => 'valor4')
     * @param int $offset Offset pagination
     * @param int $limit Limit pagination
     *
     * @return array Transactions array
     *
     * @throws LocalizedException $this->_validateApiResponse errors
     * @throws Exception API connection error
     *
     */
    public function getTransactions($from, $to, $filters = null, $offset = null, $limit = null)
    {
        $this->_debug('-- Get Transactions --');
        $this->_debug('- Input -');
        $this->_debug('From:');
        $this->_debug($from);
        $this->_debug('To:');
        $this->_debug($to);
        $this->_debug('Filters:');
        $this->_debug($filters);
        $this->_debug('Offset:');
        $this->_debug($offset);
        $this->_debug('Limit:');
        $this->_debug($limit);

        try {
            $api      = $this->_getApi();
            $response = $api->getTransactions($from, $to, $filters, $offset, $limit);

            $this->_debug('- Response -');
            $this->_debug($response);

            $this->_validateApiResponse($response);
        }
        catch (LocalizedException $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }
        catch (Exception $e) {
            $this->_debug($e->getMessage());
            throw $e;
        }

        return $response['datos'];
    }

    /**
     *
     * Get Payer Identifier Attribute
     *
     * @return string
     *
     */
    public function getPayerIdentifierAttribute()
    {
        return $this->getConfigData(self::CD_CONFIG_GROUP_ID, 'payer_identifier_attribute');
    }

    /**
     *
     * Get CobroDigital Attributes with Payment Form Labels relationship
     *
     * @return string
     *
     */
    public function getAttributesLabelsMap()
    {
        return $this->getConfigData(self::CD_CONFIG_GROUP_ID, 'map_attribute_label');
    }

    /**
     *
     * Get CobroDigital config data
     *
     * @param string $groupId system config group ID
     * @param string $fieldId system config field ID
     *
     * @return string
     *
     */
    public function getConfigData($groupId, $fieldId)
    {
        return $this->_scopeConfig->getValue(sprintf('payment/%s/%s', $groupId, $fieldId), ScopeInterface::SCOPE_WEBSITES, $this->_getWebsiteId());
    }

    /**
     *
     * Return API Class to interact with CobroDigital system
     *
     * @return \Cmdepi\CobroDigital\Api Class to interact with CobroDigital API
     *
     * @throws LocalizedException API Initialization errors
     *
     */
    protected function _getApi()
    {
        $url        = $this->_getUrl();
        $idComercio = $this->_getIdComercio();
        $sid        = $this->_getSid();

        if ($url && $idComercio && $sid) {
            return $this->_apiFactory->create(['url' => $url, 'idComercio' => $idComercio, 'sid' => $sid]);
        }

        throw new LocalizedException(__('API Init Parameters Error'));
    }

    /**
     *
     * Get website ID to use on config values scope getter
     *
     * @return int|string
     *
     */
    protected function _getWebsiteId()
    {
        /**
         *
         * @note If there is no custom website ID set, we are going to use environment website ID
         *
         */
        if (is_null($this->_websiteId)) {
            return $this->_storeManager->getStore()->getWebsiteId();
        }

        return $this->_websiteId;
    }

    /**
     *
     * Get API URL
     *
     * @return string
     *
     */
    protected function _getUrl()
    {
        return $this->getConfigData(self::CD_CONFIG_GROUP_ID, 'url');
    }

    /**
     *
     * Get 'idComercio' credential to interact with CobroDigital API
     *
     * @return string
     *
     */
    protected function _getIdComercio()
    {
        return $this->getConfigData(self::CD_CONFIG_GROUP_ID, 'id_comercio');
    }

    /**
     *
     * Get 'sid' credential to interact with CobroDigital API
     *
     * @return string
     *
     */
    protected function _getSid()
    {
        return $this->getConfigData(self::CD_CONFIG_GROUP_ID, 'sid');
    }


    /**
     *
     * Determine if is enable the debug feature
     *
     * @return boolean
     *
     */
    protected function _isDebugEnable()
    {
        return $this->getConfigData(self::CD_CONFIG_GROUP_ID, 'debug');
    }

    /**
     *
     * Log debug data to file if debug flag is enable
     *
     * @param mixed $debugData
     *
     * @return void
     *
     */
    protected function _debug($debugData)
    {
        if($this->_isDebugEnable()) {
            $this->_logger->info(var_export($debugData, true));
        }
    }

    /**
     *
     * Validate if api resource interaction runs correctly
     *
     * @param array $response API response. The array has the form ('ejecucion_correcta' => 'determine if runs ok or not', 'log' => 'log information', 'data' => 'response data if exists')
     *
     * @return void
     *
     * @throws LocalizedException API Response errors
     *
     */
    protected function _validateApiResponse($response)
    {
        /**
         *
         * @note If there is an error with the connection with the API or in the response format
         *
         */
        if (!is_array($response) || !isset($response['ejecucion_correcta'])) {
            throw new LocalizedException(__('API Response format error'));
        }
    }
}
