<?php
/**
 *
 * @description Payer Model (model to work with 'cd_payer' table)
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Cmdepi\CobroDigital\Model\CdFactory;
use Cmdepi\CobroDigital\Model\PayerFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class PaymentTicket extends \Magento\Framework\Model\AbstractModel implements \Cmdepi\CobroDigital\Api\Data\PaymentTicketInterface
{
    /**
     *
     * System Config Group ID
     *
     * @const PAYMENT_TICKET_CONFIG_GROUP_ID
     *
     */
    const PAYMENT_TICKET_CONFIG_GROUP_ID = 'cdticket';

    /**
     *
     * CobroDigital Model Factory
     *
     * @var CdFactory
     *
     */
    protected $_cdFactory;

    /**
     *
     * Payer Model Factory
     *
     * @var PayerFactory
     *
     */
    protected $_payerFactory;

    /**
     *
     * @var DateTime
     *
     */
    protected $_date;

    /**
     *
     * CobroDigital Model
     *
     * @var Cd
     *
     */
    protected $_cd = null;

    /**
     *
     * Payer Model
     *
     * @var Payer
     *
     */
    protected $_payer = null;

    /**
     *
     * Constructor
     *
     * @param CdFactory $cdFactory
     * @param PayerFactory $payerFactory
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     *
     */
    public function __construct(
        CdFactory $cdFactory,
        PayerFactory $payerFactory,
        DateTime $date,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        /**
         *
         * Set CobroDigital Factory Model to interact with CobroDigital API
         *
         */
        $this->_cdFactory = $cdFactory;

        /**
         *
         * Set Payer Factory Model
         *
         */
        $this->_payerFactory = $payerFactory;

        /**
         *
         * Get date
         *
         */
        $this->_date = $date;

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     *
     * Initialize model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init(\Cmdepi\CobroDigital\Model\ResourceModel\PaymentTicket::class);
    }
    /**
     *
     * Get payment ticket ID
     *
     * @return int|null
     *
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     *
     * Get CobroDigital Payment Ticket Identifier
     *
     * @return int|string
     *
     */
    public function getNroBoleta()
    {
        return $this->getData(self::NRO_BOLETA);
    }

    /**
     *
     * Get Payment Ticket Created At
     *
     * @return string
     *
     * @note It does not have a set method because it is calculated before save action
     *
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     *
     * Get Payment Ticket Due Date
     *
     * @return string
     *
     * @note It does not have a set method because it is calculated before save action
     * @note It has the 'Ymd' format
     *
     */
    public function getDueDate()
    {
        return $this->getData(self::DUE_DATE);
    }

    /**
     *
     * Get Payment Ticket Amount
     *
     * @return int
     *
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }

    /**
     *
     * Get Payment Ticket Payer ID
     *
     * @return int|string
     *
     */
    public function getPayerId()
    {
        return $this->getData(self::PAYER_ID);
    }

    /**
     *
     * Get payment ticket ID
     *
     * @param int $id
     *
     * @return $this
     *
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     *
     * Set CobroDigital Payment Ticket Identifier
     *
     * @param int|string $nroBoleta
     *
     * @return $this
     *
     */
    public function setNroBoleta($nroBoleta)
    {
        return $this->setData(self::NRO_BOLETA, $nroBoleta);
    }

    /**
     *
     * Set Payment Ticket Amount
     *
     * @param int $amount
     *
     * @return $this
     *
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     *
     * Set Payment Ticket Payer Id
     *
     * @param int|string $payerId
     *
     * @return $this
     *
     */
    public function setPayerId($payerId)
    {
        return $this->setData(self::PAYER_ID, $payerId);
    }

    /**
     *
     * Get payer related to the payment ticket
     *
     * @return Payer
     *
     * @todo Improve. Use Repositories.
     *
     */
    public function getPayer()
    {
        return $this->_getPayer()->load($this->getPayerId());
    }

    /**
     *
     * Validate If Payment Ticket Is Paid
     *
     * @return bool
     *
     */
    public function isPaid()
    {
        /**
         *
         * @note Get successful transactions and validate if this payment method is returned
         *
         */
        $transactions = $this->_getCd()->getTransactions($this->_date->date('Ymd'), $this->getDueDate(), array('nro_boleta' => $this->getNroBoleta()));
        if (count($transactions)) {
            return true;
        }

        return false;
    }

    /**
     *
     * Get Payment Ticket HTML
     *
     * @return string
     *
     */
    public function getHtml()
    {
        return $this->_getCd()->getPaymentTicketHTML($this->getNroBoleta());
    }

    /**
     *
     * Cancel Payment Ticket
     *
     * @return string
     *
     */
    public function cancel()
    {
        return $this->_getCd()->disablePaymentTicket($this->getNroBoleta());
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return Cd
     *
     */
    protected function _getCd()
    {
        if (is_null($this->_cd)) {
            $this->_cd = $this->_cdFactory->create();
        }

        return $this->_cd;
    }

    /**
     *
     * Get Payer Model
     *
     * @return Payer
     *
     */
    protected function _getPayer()
    {
        if (is_null($this->_payer)) {
            $this->_payer = $this->_payerFactory->create();
        }

        return $this->_payer;
    }
}
