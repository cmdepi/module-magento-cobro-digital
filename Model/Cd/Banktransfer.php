<?php
/**
 *
 * @description CobroDigital Payment Ticket Model
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\Cd;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Payment\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Cmdepi\CobroDigital\Model\BanktransferFactory as BanktransferFactoryModel;
use Cmdepi\CobroDigital\Model\Banktransfer        as BanktransferModel;

class Banktransfer extends \Magento\Payment\Model\Method\AbstractMethod
{
    /**
     *
     * Payment method code (use in system.xml -> system -> section 'payment')
     *
     * @const CODE
     *
     */
    const CODE = 'cdbanktransfer';

    /**
     *
     * Payment method code
     *
     * @var string
     *
     */
    protected $_code = self::CODE;

    /**
     *
     * Form block path
     *
     * @var string
     *
     */
    protected $_formBlockType = \Cmdepi\CobroDigital\Block\Form\Banktransfer::class;

    /**
     *
     * Instructions block path
     *
     * @var string
     *
     */
    protected $_infoBlockType = \Cmdepi\CobroDigital\Block\Info\Banktransfer::class;

    /**
     *
     * Set is offline payment method to true
     *
     * @var bool
     *
     */
    protected $_isOffline = true;

    /**
     *
     * Bank Transfer Factory Model
     *
     * @var BanktransferFactoryModel
     *
     */
    protected $_bankTransferFactory;

    /**
     *
     * Bank Transfer Model
     *
     * @var BanktransferModel
     *
     */
    protected $_bankTransfer = null;

    /**
     *
     * Constructor
     *
     * @param BanktransferFactoryModel $bankTransferFactory
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     */
    public function __construct(
        BanktransferFactoryModel $bankTransferFactory,
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        /**
         *
         * Set CobroDigital bank transfer factory model to interact with CobroDigital API
         *
         */
        $this->_bankTransferFactory = $bankTransferFactory;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     *
     * Assign data to info model instance
     *
     * @param array|\Magento\Framework\DataObject $data
     *
     * @return \Magento\Payment\Model\Method\AbstractMethod
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @api
     *
     * @deprecated 100.2.0
     *
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        /**
         *
         * @note Get payment instance model and quote instance model
         *
         */
        /** @var \Magento\Quote\Model\Quote\Payment $payment */
        $payment = $this->getInfoInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $payment->getQuote();

        /**
         *
         * @note Get payer data
         *
         */
        $payerData = $data->getData('additional_data');

        /**
         *
         * Transfer operation
         *
         * @note If it does not run ok, thrown an error
         *
         */
        $this->_getBanktransferModel()->transfer($payerData['firstname'], $payerData['lastname'], $payerData['cuit'], $payerData['email'], $payerData['cbu'], $quote->getGrandTotal(), $payerData['payment_installments']);

        /**
         *
         * @note Set payer data to quote/order payment 'additional_information' column
         *
         */
        foreach ($payerData as $key => $value) {
            $payment->setAdditionalInformation($key, $value);
        }

        return parent::assignData($data);
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return BanktransferModel
     *
     */
    protected function _getBanktransferModel()
    {
        if (is_null($this->_bankTransfer)) {
            $this->_bankTransfer = $this->_bankTransferFactory->create();
        }

        return $this->_bankTransfer;
    }
}
