<?php
/**
 *
 * @description CobroDigital Ticket Payment Method Info Block
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Block\Info;

use DateTime;
use Cmdepi\CobroDigital\Model\Payer;
use Cmdepi\CobroDigital\Model\PaymentTicket;

class Ticket extends \Magento\Payment\Block\Info
{
    /**
     *
     * @var string
     *
     */
    protected $_template = 'info/ticket.phtml';

    /**
     *
     * Get Payment Ticket Due Date
     *
     * @return string
     *
     */
    public function getPaymentTicketDueDate()
    {
        $paymentTicket = $this->getPaymentTicket();
        $date          = DateTime::createFromFormat('Ymd', $paymentTicket->getDueDate());
        return $date->format('d/m/Y');
    }

    /**
     *
     * Get download ticket URL
     *
     * @return string
     *
     */
    public function getPaymentTicketPrintUrl()
    {
        $paymentTicket   = $this->getPaymentTicket();
        $paymentTicketId = $paymentTicket->getId();
        $urlPath         = sprintf('cmdepicd/sales/printticket/ticket/%s', $paymentTicketId);

        return $this->getUrl($urlPath);
    }

    /**
     *
     * Get payment ticket
     *
     * @return PaymentTicket
     *
     */
    public function getPaymentTicket()
    {
        /** @var PaymentTicket $paymentTicket */
        $paymentTicket = $this->getInfo()->getOrder()->getPaymentTicket();
        return $paymentTicket;
    }

    /**
     *
     * Get payer
     *
     * @return Payer
     *
     */
    public function getPayer()
    {
        /** @var Payer $payer */
        $payer = $this->getInfo()->getOrder()->getPayer();
        return $payer;
    }
}
