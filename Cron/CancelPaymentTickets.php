<?php
/**
 *
 * @description Cancel Payment Tickets Expired Cron
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Cron;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\DB\Transaction;
use Cmdepi\CobroDigital\Model\Cd\Ticket;
use Cmdepi\CobroDigital\Model\Sales\Order;
use Cmdepi\CobroDigital\Logger\Logger;

class CancelPaymentTickets
{
    /**
     *
     * @var Collection
     *
     */
    protected $_collection;

    /**
     *
     * @var DateTime
     *
     */
    protected $_date;

    /**
     *
     * @var InvoiceService
     *
     */
    protected $_invoiceService;

    /**
     *
     * @var InvoiceSender
     *
     */
    protected $_invoiceSender;

    /**
     *
     * @var Transaction
     *
     */
    protected $_transaction;

    /**
     *
     * @var Logger
     *
     */
    protected $_logger;

    /**
     *
     * Class constructor
     *
     * @param CollectionFactory $collectionFactory
     * @param DateTime $date
     * @param InvoiceService $invoiceService
     * @param InvoiceSender $invoiceSender
     * @param Transaction $transaction
     * @param Logger $logger
     *
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        DateTime $date,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        Transaction $transaction,
        Logger $logger
    ) {
        /**
         *
         * Get collection
         *
         */
        $this->_collection = $collectionFactory->create();

        /**
         *
         * Get date
         *
         */
        $this->_date = $date;

        /**
         *
         * Invoice service
         *
         */
        $this->_invoiceService = $invoiceService;

        /**
         *
         * Invoice sender
         *
         */
        $this->_invoiceSender = $invoiceSender;

        /**
         *
         * Transaction service
         *
         */
        $this->_transaction = $transaction;

        /**
         *
         * Set Logger
         *
         */
        $this->_logger = $logger;
    }

    /**
     *
     * Execute cron
     *
     * @return void
     *
     */
    public function execute()
    {
        $this->_log('Init Process: ' . $this->_date->gmtDate('Y-m-d H:i:s'));

        $this->_collection
            /**
             *
             * @note Join with payment collection
             *
             */
            ->join(array('payment' => 'sales_order_payment'), 'main_table.entity_id = payment.parent_id', '*')
            /**
             *
             * @note Filter by payment ticket method
             *
             */
            ->addFieldToFilter('payment.method', array('eq' => Ticket::CODE))
            /**
             *
             * @note Filter by 'pending' orders
             *
             */
            ->addFieldToFilter('status', array('eq' => 'pending'))
            /**
             *
             * @note Load collection
             *
             */
            ->load();

        try {
            /** @var Order $order */
            foreach ($this->_collection->getItems() as $order) {
                $this->_log(sprintf('--------------------- Order ID #%s ---------------------', $order->getIncrementId()));

                $paymentTicket = $order->getPaymentTicket();
                if ($paymentTicket) {
                    if ($paymentTicket->isPaid()) {
                        $this->_log('Order is paid and it is going to have an invoice!');
                        $this->_createInvoiceFromOrder($order);
                    }
                    else {
                        $today   = strtotime($this->_date->gmtDate('Ymd'));
                        $dueDate = strtotime($paymentTicket->getDueDate());

                        /**
                         *
                         * @note If today date is less than due date then the ticket expired
                         *
                         */
                        if (($today - $dueDate) < 0) {
                            $this->_log('Order is going to be canceled!');
                            $order->cancel();
                        }
                    }
                }

                $this->_log(sprintf('--------------------- Order ID #%s ---------------------', $order->getIncrementId()));
            }
        }
        catch (LocalizedException $e) {
            $this->_log('Error: ');
            $this->_log($e->getMessage());
        }
        catch (\Exception $e) {
            $this->_log('Error: ');
            $this->_log($e->getMessage());
        }
    }

    /**
     *
     * Create order from invoice
     *
     * @param Order $order
     *
     * @return void
     *
     */
    private function _createInvoiceFromOrder(Order $order)
    {
        if($order->canInvoice()) {
            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->register();
            $invoice->save();

            $transactionSave = $this->_transaction->addObject($invoice)
                                                  ->addObject($invoice->getOrder());
            $transactionSave->save();

            $this->_invoiceSender->send($invoice);

            $order->addStatusHistoryComment(_('Notified customer about invoice #%1.', $invoice->getId()))
                  ->setIsCustomerNotified(true)
                  ->save();
        }
    }

    /**
     *
     * Log message
     *
     * @param $message
     *
     */
    private function _log($message)
    {
        $this->_logger->info($message);
    }
}