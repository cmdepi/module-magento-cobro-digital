<?php
/**
 *
 * @description CobroDigital Ticket Payment Method Form Block
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Block\Form;

use Magento\Framework\View\Element\Template\Context;
use Cmdepi\CobroDigital\Model\Payer\AttributeFactory;
use Cmdepi\CobroDigital\Model\Payer\Attribute;

class Ticket extends \Magento\Payment\Block\Form
{
    /**
     *
     * @var AttributeFactory
     *
     */
    protected $_payerAttributeFactory;

    /**
     *
     * @var Attribute
     *
     */
    protected $_payerAttribute = null;

    /**
     *
     * @var string
     *
     */
    protected $_template = 'form/ticket.phtml';

    /**
     *
     * Constructor
     *
     * @param AttributeFactory $payerAttributeFactory
     * @param Context $context
     * @param array $data
     *
     */
    public function __construct(
        AttributeFactory $payerAttributeFactory,
        Context $context,
        array $data = []
    )
    {
        $this->_payerAttributeFactory = $payerAttributeFactory;

        parent::__construct($context, $data);
    }

    /**
     *
     * Get form attributes
     *
     * @return array
     *
     */
    public function getFormAttributes()
    {
        return $this->_getPayerAttribute()->getMappedAttributes();
    }

    /**
     *
     * Get attribute value from info instance
     *
     * @param string $key
     *
     * @return string
     *
     */
    public function getAttributeValue($key)
    {
        return $this->getMethod()->getInfoInstance()->getAdditionalInformation($key);
    }

    /**
     *
     * Get payer id
     *
     * @return null|string
     *
     */
    public function getPayerId()
    {
        /** @var \Magento\Sales\Model\Order\Payment $info */
        $info     = $this->getMethod()->getInfoInstance();
        /** @var \Cmdepi\CobroDigital\Model\Customer\Data\Customer $customer */
        $customer = $info->getOrder()->getCustomer();

        if ($customer->getId()) {
            return $customer->getPayer()->getId();
        }

        return $info->getOrder()->getCustomer();
    }

    /**
     *
     * Get payer attribute
     *
     * @return Attribute
     *
     */
    protected function _getPayerAttribute()
    {
        if (is_null($this->_payerAttribute)) {
            $this->_payerAttribute = $this->_payerAttributeFactory->create();
        }

        return $this->_payerAttribute;
    }
}
