<?php
/**
 *
 * @description Resource model to work with 'cd_payer' table
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Model\ResourceModel;

use Cmdepi\CobroDigital\Model\CdFactory;
use Cmdepi\CobroDigital\Model\Cd;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Cmdepi\CobroDigital\Model\Payer as PayerModel;
use Cmdepi\CobroDigital\Helper\Payer as PayerHelper;

class Payer extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * CobroDigital Model
     *
     * @var CdFactory
     *
     */
    protected $_cdFactory;

    /**
     *
     * CobroDigital Model
     *
     * @var Cd
     *
     */
    protected $_cd = null;

    /**
     *
     * Payer Helper
     *
     * @var PayerHelper
     *
     */
    protected $_payerHelper;

    /**
     *
     * @var SerializerInterface
     *
     */
    private $_serializer;

    /**
     *
     * Constructor
     *
     * @param CdFactory $cdFactory
     * @param PayerHelper $payerHelper
     * @param SerializerInterface $serializer
     * @param Context $context
     * @param string $connectionName
     *
     */
    public function __construct(
        CdFactory $cdFactory,
        PayerHelper $payerHelper,
        SerializerInterface $serializer,
        Context $context,
        $connectionName = null
    )
    {
        /**
         *
         * Set CobroDigital factory model to interact with CobroDigital API
         *
         */
        $this->_cdFactory = $cdFactory;

        /**
         *
         * Set Payer Helper
         *
         */
        $this->_payerHelper = $payerHelper;

        /**
         *
         * Set serialize/unserialize feature
         *
         */
        $this->_serializer = $serializer;

        /**
         *
         * Call parent constructor
         *
         */
        parent::__construct($context, $connectionName);
    }

    /**
     *
     * Initialize resource model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init('cd_payer', 'entity_id');
    }

    /**
     *
     * Perform actions before object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        /**
         *
         * @note Get Payer information to send to CobroDigital API. See $this->_afterSave().
         *
         */
        $information = $object->getInformation();

        /**
         *
         * @note Set payer identifier value if it is a new object/payer
         *
         */
        if ($object->isObjectNew()) {
            $identifier = $this->_payerHelper->createIdentifier();
            $object->setData(PayerModel::IDENTIFIER, $identifier);
        }

        /**
         *
         * @note Set to payer information to send to CobroDigital API. See $this->_afterSave().
         * @note To edit payers it is not necessary send this data to CobroDigital API but we need to persist it in our database table to maintain the consistency of our data
         *
         */
        $information[$object->getIdentifierAttribute()] = $object->getIdentifier();

        /**
         *
         * @note Serialize 'information' data to save it
         *
         */
        $object->setData(PayerModel::INFORMATION, $this->_serializer->serialize($information));

        return parent::_beforeSave($object);
    }

    /**
     *
     * Perform actions after object save
     *
     * @param \Magento\Framework\Model\AbstractModel|\Magento\Framework\DataObject $object
     *
     * @return $this
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        /**
         *
         * @note Get Payer information to send
         *
         */
        $information = $this->_serializer->unserialize($object->getInformation());

        /**
         *
         * @note Create/Edit Payer in CobroDigital system
         *
         */
        if ($this->_getCd()->payerExists($object->getIdentifierAttribute(), $object->getIdentifier())) {
            $this->_getCd()->editPayer($object->getIdentifierAttribute(), $object->getIdentifier(), $information);
        }
        else {
            $this->_getCd()->createPayer($information);
        }

        return $this;
    }

    /**
     *
     * Processing object after load data
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\AbstractDb
     *
     */
    protected function _afterLoad(\Magento\Framework\Model\AbstractModel $object)
    {
        /**
         *
         * @note Unserialize 'information' data to work with it as an array
         *
         */
        $information = $object->getData(PayerModel::INFORMATION);
        if ($information) {
            $object->setData(PayerModel::INFORMATION, $this->_serializer->unserialize($information));
        }

        return parent::_afterLoad($object);
    }

    /**
     *
     * Get CobroDigital model to interact with CobroDigital API
     *
     * @return Cd
     *
     */
    protected function _getCd()
    {
        if (is_null($this->_cd)) {
            $this->_cd = $this->_cdFactory->create();
        }

        return $this->_cd;
    }
}