<?php
/**
 *
 * @description Payer Model Interface
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Api\Data;

interface PayerInterface
{
    /**
     *
     * Payer ID
     *
     * @const ID
     *
     */
    const ID = 'entity_id';

    /**
     *
     * CobroDigital Payer Identifier
     *
     * @const ID
     *
     */
    const IDENTIFIER = 'identifier';

    /**
     *
     * Payer Information
     *
     * @const INFORMATION
     *
     */
    const INFORMATION = 'information';

    /**
     *
     * Get payer id
     *
     * @return int|null
     *
     */
    public function getId();

    /**
     *
     * Set payer id
     *
     * @param int $id
     *
     * @return $this
     *
     */
    public function setId($id);

    /**
     *
     * Get CobroDigital identifier attribute value
     *
     * @return int|null
     *
     * @note It does not have a set method because it is calculated before save action
     *
     */
    public function getIdentifier();

    /**
     *
     * Get payer information
     *
     * @return int|null
     *
     */
    public function getInformation();

    /**
     *
     * Set payer information. CobroDigital Payer data (see CobroDigital payer structure)
     *
     * @param array $information
     *
     * @return $this
     *
     * @note It is going to have an array with CobroDigital payer structure attribute values. It could have or could not have the payer identifier value. It is not necessary but has to have all other payer structure attributes values. See \Cmdepi\CobroDigital\Model\ResourceModel\Payer->_beforeSave()
     *
     */
    public function setInformation(array $information);
}
