<?php
/**
 *
 * @description CobroDigital PaymentTicket Helper
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com> *
 */
namespace Cmdepi\CobroDigital\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\RequestInterface;

class PaymentTicket extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     * Print JS Feature
     *
     * @const PRINT_JS
     *
     */
    const PRINT_JS = <<<HTML
        <script type="text/javascript">window.print()</script>
HTML;

    /**
     *
     * @var Repository
     *
     */
    protected $_assetRepo;

    /**
     *
     * @var RequestInterface
     *
     */
    protected $_request;

    /**
     *
     * Class constructor
     *
     *
     * @param Context $context
     * @param Repository $assetRepo
     * @param RequestInterface $request
     *
     */
    public function __construct(
        Context $context,
        Repository $assetRepo,
        RequestInterface $request
    ) {
        $this->_assetRepo = $assetRepo;
        $this->_request   = $request;
        parent::__construct($context);
    }

    /**
     *
     * Get body content from payment ticket HTML (HTML from \Cmdepi\CobroDigital\Model\Cd->getPaymentTicketHTML($paymentTicketId))
     *
     * @return string
     *
     */
    public function getBodyContentFromTicketHtml($paymentTicketHtml)
    {
        return $this->_getContent($paymentTicketHtml, '<body>', '</body>');
    }

    /**
     *
     * Get payment ticket HTML with print feature
     *
     * @return string
     *
     */
    public function getPaymentTicketPrintWindowHtml($paymentTicketHtml)
    {
        /**
         *
         * @note Add Title
         *
         */
        $ticketHtml = $this->_addTitleToHtml($paymentTicketHtml);

        /**
         *
         * @note Add Print JS Feature to body content
         *
         */
        $bodyContent  = $this->getBodyContentFromTicketHtml($ticketHtml);
        $bodyContent .= self::PRINT_JS;

        /**
         *
         * @note Get before body HTML
         *
         */
        $end        = strpos($ticketHtml, '<body>') + strlen('<body>');
        $beforeBody = substr($ticketHtml, 0, $end);

        return $beforeBody . $bodyContent . '</body></html>';
    }

    /**
     *
     * Add title to HTML
     *
     * @param string $paymentTicketHtml
     *
     * @return string
     *
     */
    private function _addTitleToHtml($paymentTicketHtml)
    {
        /**
         *
         * @note Get head content
         *
         */
        $head = $this->_getContent($paymentTicketHtml, '<head>', '</head>');

        /**
         *
         * @note Add title
         *
         */
        $head .= '<title>' . __('CobroDigital | Payment Ticket') . '</title>';

        /**
         *
         * @note Add favicon
         *
         */
        $head .= $this->_getFaviconHtml();

        /**
         *
         * @note Get before head content
         *
         */
        $endBeforeHead = strpos($paymentTicketHtml, '<head>') + strlen('<head>');
        $beforeHead    = substr($paymentTicketHtml, 0, $endBeforeHead);

        /**
         *
         * @note Get After head content
         *
         */
        $startAfterHead = strpos($paymentTicketHtml, '</head>');
        $endAfterHead   = strlen($paymentTicketHtml);
        $afterHead      = substr($paymentTicketHtml, $startAfterHead, $endAfterHead - $startAfterHead);

        /**
         *
         * @note Rearm
         *
         */
        $html = $beforeHead . $head . $afterHead;

        return $html;

    }

    /**
     *
     * Get content from section
     *
     * @param string $html HTML doc
     * @param string $sectionStart HTML section start tag
     * @param string $sectionEnd HTML section end tag
     *
     * @return string
     *
     */
    private function _getContent($html, $sectionStart, $sectionEnd)
    {
        $start  = strpos($html, $sectionStart);
        $start += strlen($sectionStart);
        $end    = strpos($html, $sectionEnd);

        $content = substr($html, $start, $end - $start);

        return $content;
    }

    /**
     *
     * Get favicon HTML
     *
     * @return string
     *
     */
    private function _getFaviconHtml()
    {
        $params = array('_secure' => $this->_request->isSecure());
        return sprintf('<link rel="icon" type="image/x-icon" href="%s">', $this->_assetRepo->getUrlWithParams('Magento_Theme::favicon.ico', $params));
    }
}
