<?php
/**
 *
 * @description CobroDigital Print Payment Ticket Page
 *
 * @author C. M. de Picciotto <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital\Controller\Sales;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Cmdepi\CobroDigital\Model\PaymentTicketFactory;
use Cmdepi\CobroDigital\Model\PaymentTicket;
use Cmdepi\CobroDigital\Helper\PaymentTicket as PaymentTicketHelper;

class Printticket extends \Magento\Framework\App\Action\Action
{
    /**
     *
     * @var ResponseInterface
     *
     */
    protected $_response;

    /**
     *
     * Payment Ticket Model Factory
     *
     * @var PaymentTicketFactory
     *
     */
    protected $_paymentTicketFactory;

    /**
     *
     * @var PaymentTicketHelper
     *
     */
    protected $_paymentTicketHelper;

    /**
     *
     * Payment Ticket Model
     *
     * @var PaymentTicket
     *
     */
    protected $_paymentTicket = null;

    /**
     *
     * Constructor
     *
     * @param ResponseInterface $response
     * @param PaymentTicketFactory $paymentTicketFactory
     * @param PaymentTicketHelper $paymentTicketHelper
     * @param Context $context
     *
     */
    public function __construct(
        ResponseInterface $response,
        PaymentTicketFactory $paymentTicketFactory,
        PaymentTicketHelper $paymentTicketHelper,
        Context $context
    ) {
        /**
         *
         * Set Response Object
         *
         */
        $this->_response = $response;

        /**
         *
         * Set Payment Ticket Factory Model
         *
         */
        $this->_paymentTicketFactory = $paymentTicketFactory;

        /**
         *
         * Set Payment Ticket Helper
         *
         */
        $this->_paymentTicketHelper = $paymentTicketHelper;

        parent::__construct($context);
    }

    /**
     *
     * Download Payment Ticket
     *
     * @return \Magento\Framework\App\ResponseInterface
     *
     */
    public function execute()
    {
        $paymentTicketId = $this->getRequest()->getParam('ticket');
        $paymentTicket   = $this->_getPaymentTicket()->load($paymentTicketId);
        $html            = $this->_getPaymentTicketPrintHtml($paymentTicket->getHtml());

        $this->_response->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'text/html', true)
            ->setHeader('Content-Length', strlen($html), true)
            ->setHeader('Last-Modified', date('r'), true)
            ->setBody($html);

        return $this->_response;
    }

    /**
     *
     * Get Payment Ticket Model
     *
     * @return PaymentTicket
     *
     */
    protected function _getPaymentTicket()
    {
        if (is_null($this->_paymentTicket)) {
            $this->_paymentTicket = $this->_paymentTicketFactory->create();
        }

        return $this->_paymentTicket;
    }

    /**
     *
     * Add Print JS feature to HTML
     *
     * @param string $paymentTicketHtml
     *
     * @return string
     *
     */
    private function _getPaymentTicketPrintHtml($paymentTicketHtml)
    {
        return $this->_paymentTicketHelper->getPaymentTicketPrintWindowHtml($paymentTicketHtml);
    }
}
